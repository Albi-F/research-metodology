\documentclass{article}

\usepackage{authoraftertitle}
\title{How CoPilot assist task completion?}
\author{Alberto Fabbri}
\date{26/10/2022}

\usepackage[english]{babel}
\usepackage[document]{ragged2e}

\usepackage{svg}
% image folder path
\graphicspath{ {images/} }
\usepackage{wrapfig}

\usepackage[sorting=none]{biblatex}
\usepackage{csquotes}
\addbibresource{references.bib}

\usepackage{quoting}
\usepackage{caption}  


\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=black,
    filecolor=magenta,
    urlcolor=blue,
    citecolor=black,
}

\usepackage{geometry}
\geometry{
    a4paper,
    left=17mm,
    right=23mm,
    top=20mm,
    bottom=20mm,
    footskip=30.0pt,
}

% set indentation length
\setlength{\parindent}{2em}
% indent the first paragraph of the sections
\usepackage{indentfirst}
 
\usepackage{listings}

% colored link in HKR logo
\usepackage{xcolor}
\definecolor{HKR}{RGB}{00, 170, 50}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\normalsize\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\normalsize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle}

\newcommand{\coloredURL}[2][blue]{\url{#2}{\color{#1}}}

% change title of list of figures/tables
\addto\captionsenglish{\renewcommand{\listfigurename}{Figures}}
\addto\captionsenglish{\renewcommand{\listtablename}{Tables}}

\usepackage{acro}

\usepackage{tikz}
\usetikzlibrary{shapes.geometric, arrows, positioning}

\tikzstyle{startstop} = [rectangle, rounded corners, minimum width=3cm, minimum height=1cm,text centered, draw=black, fill=red!30]
\tikzstyle{io} = [trapezium, trapezium left angle=70, trapezium right angle=110, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=blue!30]
\tikzstyle{process} = [rectangle, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=orange!30]
\tikzstyle{decision} = [diamond, minimum width=3cm, minimum height=1cm, text centered, draw=black, fill=green!30]
\tikzstyle{arrow} = [thick,->,>=stealth]


\DeclareAcronym{ai}{
  short=AI,
  long=Artificial Intelligence,
}
\DeclareAcronym{ml}{
  short=ML,
  long=Machine Learning,
}
\DeclareAcronym{dl}{
  short=DL,
  long=Deep Learning,
}

\usepackage{array}
\usepackage{supertabular}
\usepackage{makecell}

\begin{document}

\setcellgapes{10pt}
\makegapedcells
	
\begin{figure}[h!]
   \minipage{0.50\textwidth}
		\includesvg{HKR.svg}\\
		\small
		Kristianstad University\\
        SE-291 88 Kristianstad\\
        Sweden\\
        \href{tel:+46 44 20 30 00}{\color{black}{+46 44 20 30 00}}\\
        \textbf{\href{www.hkr.se}{\color{HKR}{www.hkr.se}{}}}
   \endminipage
\end{figure}
	
\Large

\textbf{\\
Bachelor’s in software development\\
Faculty of Computer Science\\
% DA256D HT21 Algorithms and Data Structures
}

\begin{center}
\vspace{4cm}
\Huge
\MyTitle \\
\vspace{1.5cm}
\LARGE
\MyAuthor \\
\MyDate
\end{center}

% no numeric for this page
\thispagestyle{empty}

\newpage
\raggedright\textbf{Author}\\
\MyAuthor
\vspace{5mm}

\raggedright\textbf{Title}\\
\MyTitle
\vspace{5mm}

\raggedright\textbf{Supervisor}\\
Charlotte Sennersten
\vspace{5mm}

\raggedright\textbf{Examiner}\\
Kamilla Klonowska
\vspace{5mm}

\raggedright\textbf{Abstract}\\
\justifying\noindent
Artificial Intelligence is innovating in more and more fields but until very recently nobody had tried to use it to improve the productivity of software developers. This paper analyze this possibility and the challenges that it brings with it. The final goal is to understand if this new technological development allows developers to write code faster while retaining a high quality.
\vspace{5mm}

\raggedright\textbf{Keywords}\\
\noindent
Machine Learning, Software Development, GitHub CoPilot, code auto-completion, GPT-3.
\thispagestyle{empty}


\newpage
\tableofcontents
\large
\thispagestyle{empty}

\newpage
\listoffigures
\listoftables
\lstlistoflistings
\thispagestyle{empty}

\newpage
\section{Introduction}

% Here you describe the area you find being interesting and start to lay a background for why this is important, why it is of concern to you and what difference it might have and also benefit of it. 
% Here you also bring in your articles and refer to them and can then take snippets of the text you find support your case/area and argue and cite what this article supports or argues against and use then brackets and numbering which refers to your reference list at the back of your project plan. 
% Further below you narrow this down to be more focused.

The world relies more and more on programs and automation in order to overcome the challenges laying ahead of us. Furthermore automation allows us to improve the efficiency of our processes thus reducing the impact humans have on the environment. Automation as such is sought as much as possible but given that it usually requires writing complex computer programs, its development involves the writing of a lot of code. Creating a program from scratch requires a great amount of time even if the program in itself is not particularly innovative or disruptive because coding often involves the writing of multiple functions very similar between them \cite{hillAutomaticMethodCompletion2004}\cite{kimMachineLearningBasedCode2020}.

\begin{quoting}[font=itshape, begintext={``}, endtext={''\cite{hillAutomaticMethodCompletion2004}}]
When a programmer requires an idiomatic unit of implementation he/she will take one of two approaches: search through the existing source to find an appropriate implementation to copy/paste or write the method from scratch. Both approaches are time-consuming and the second may introduce inconsistencies between these atomic units. The authors’ contribution is a tool which facilitates a compromise between the two approaches: the programmer begins his/her implementation and the tool automatically suggests a completion for the method body. The suggested completion saves the programmer from manually searching for an implementation to copy and from reinventing the wheel.
\end{quoting}

\ac{ai} has proven to be very useful at helping specialists in speeding up their jobs for example by helping doctors to diagnose diseases faster therefore it can probably be used to help developers as well. Nevertheless its use comes with new risks and new workflows that needs to be analyzed in order to get the most benefit with the lowest risk \cite{wanHowDoesMachine2021}.\\
One of the requirements of today´s \ac{ai} algorithms is to have a large amount of data available on which to train the neural network, fortunately this is not a problem in the IT field as there are a huge quantity of open-source projects whose source code is freely available and usable. A neural network can therefore be trained by feeding to it all these programs and it should be able to solve at least some of the most recurring coding problems \cite{chenEvaluatingLargeLanguage2021}\cite{pearceAsleepKeyboardAssessing2021}.

\newpage
\subsection{GitHub CoPilot}
\begin{wrapfigure}{r}{0.25\textwidth} %this figure will be at the right
    \centering
    \includegraphics[width=0.20\textwidth]{GitHub CoPilot Logo}
\end{wrapfigure}
GitHub Copilot is an artificial intelligence tool developed by GitHub Inc. to aid developers writing code by trying to predict what functions they need. GitHub CoPilot is based on the OpenAI Codex which in turn is a modified version of GPT-3 (Generative Pre-trained Transformer 3), a language model developed by OpenAI using deep-learning to produce human-like text\cite{krillOpenAIOffersAPI2021}.\\
GitHub Copilot helps programmers by converting code comments into executable code and auto-completing repetitive blocks of code and entire methods and functions.\\
\vspace{0.5cm}
Following a couple of examples of code generated by GitHub Copilot are shown:\\
\vspace{0.3cm}
In this example the only input from the programmer was the comment on the first line:
\begin{lstlisting}[language=Python, caption=GitHub CoPilot code generated from a comment]
# Get array and return number divisible by 7

def getNumbers():
    nums = []
    inp = input("Enter a number (<Enter> to quit): ")
    while inp != "":
        nums.append(eval(inp))
        inp = input("Enter a number (<Enter> to quit): ")
    return nums

def printResults(nums):
    for i in nums:
        if i % 7 == 0:
            print(i)

def main():
    nums = getNumbers()
    printResults(nums)

main()
\end{lstlisting}

\vspace{0.3cm}
In this example the only input from the programmer was the name of the first function:
\begin{lstlisting}[language=Python, caption=GitHub CoPilot code generated from a function's name]
def read_file(filename):
    with open(filename, 'r') as f:
        return f.read()

def write_file(filename, content):
    with open(filename, 'w') as f:
        f.write(content)

def main():
    content = read_file('input.txt')
    write_file('output.txt', content)

if __name__ == '__main__':
    main()
\end{lstlisting}

\newpage
\subsection{Problem Area}

% You start to narrow down like a funnel and is more onto the actual problem at this point and support this with a couple of more articles you have found.
% Again you reference these and indicate with brackets [4] and [5], ...

The use of \ac{ml} to aid in the development of software although very promising is also source of great concerns especially around its effect on the quality \cite{chenEvaluatingLargeLanguage2021} and safety \cite{pearceAsleepKeyboardAssessing2021} of the code itself. It is therefore necessary to prove its usefulness before its widespread adoption given this possible insidious drawbacks.

\vspace{0.2cm}

In order to prove the usefulness of these recent \ac{ai} aided coding technologies its most famous embodiment, GitHub CoPilot, is being analyzed. The analysis focus on the speed of completion of several tasks and looks for any difference in the time taken by developers to solve these tasks with and without the addition help 

\subsubsection{Keywords and associated articles}

% Here you can list your keywords and describe shortly why you see these being the keywords helping you find the articles or rather describe how you have performed your search of articles using these. The keywords can of course be combined and you can get different hits on these, so it is good to be precise. For example if you search on your keywords you can get 100 hits and you may choose 10 of these, it is good for the reader to understand why you did choose the 10 and how you created the criteria for this. This is so the reader understands how you have prioritized your search.
% Keyword 1, keyword 2, keyword 3, keyword 4 and keyword 5.

Finding the right search keywords has been very challenging as most of the keywords I could think of are quite generic and can be used to refer to research papers concerning completely different topics.
For this reason I decided to use the keyword "GitHub CoPilot" together with "Machine Learning" and "Software Development". Furthermore, given that CoPilot is essentially a smarter code completition tool I added the keyword "code auto-completition". Lastly "GPT-3" is the pre-trained autoregressive \ac{ml} model on which CoPilot is built on.

\newpage
\section{Research Question and Hypothes(es)}

\subsection{Why this RQ?}

% Research Question: Why does the two plants look like they should die?
% Hypothesis 1: The roots are overcrowded
% Hypothesis 2: Not enough water
% Hypothesis 3: They get too much water
% How to Formulate an Effective Research Hypothesis (= answering the RQ)
% 1. State the problem that you are trying to solve. Make sure that 
% the hypothesis clearly defines the topic and the focus of the 
% experiment.
% 2. Try to write the hypothesis as an if-then statement. ...
% 3. Define the variables.
% Experiment Design is to find data to support one or the other of the 
% hypotheses.

GitHub CoPilot has recently been released in a closed beta form and it is important to evaluate its impact in order to understand its current limits, best use-case and how it can be improved.
The current impact of such a technology on the development cycle of software is largely unknown and it can fall back to one of these possibilities:
\begin{itemize}
    \item Its use speed up the development of programs without significantly impacting the legibility, quality and security of the underlying code.
    \item Its use speed up the development but impact one or several of the characteristics mentioned above.
    \item Its use does not provide any advantage and simply slow down the developers.
\end{itemize}

\newpage
\section{Variables}
% Write your variables here and explain how they support your chosen research question.

The purpose of GitHub CoPilot is to facilitate developers' job therefore it is necessary to find and analyze metrics that gives a quantitative measure of much this software helps developers.\\
One such metric that is easily measurable is the time it takes a developer to solve a particular task.\\
To get tasks that are the right level of difficult, online websites that offer coding challenges can be used, Codeforces\cite{Codeforces} and CodeChef\cite{CodeChefCompetitiveProgramming} are two good exmaples.

\subsection{Input}
- Coding task of various difficulty
- GitHub CoPilot help

\subsection{Output}
- Time needed to solve the task

\newpage
\section{Experiment Design}
% Here you will explain and show how you will handle the implementation and in stepwise instructions.
% Overview fullstack architecture/model/algorithm
% Flowchart to demonstrate inner workings and workflow

The implementation involves the development and deployment of a custom website for the recruitment and enrollment of developers in the experiment and its execution. The website will allows the developers to connect and solve the tasks according to the metodology explained under the data collection chapter. The tasks and their solutions can be either imported from other platforms or designed from the ground up.

\vspace{0.5cm}

The following example \cite{CodeforcesProblem1739B} is a suitable task challenge from Codeforces:
\begin{center}\fbox{\begin{minipage}{40em}
For an array of non-negative integers \(a\) of size \(n\), we construct another array \(d\) as follows: \[d_1 = a_1, d_i = | a_i - a_{i−1} | : 2 \leq i \leq n\]

Your task is to restore the array \(a\) from a given array \(d\), or to report that there are multiple possible arrays.

\vspace{0.2cm}

\textbf{Input}\\
The first line contains a single integer \(t (1  \leq t  \leq 100)\) — the number of test cases.

The first line of each test case contains one integer \(n (1 \leq n  \leq 100)\) — the size of the arrays \(a\) and \(d\).

The second line contains n integers \(d_1,d_2,...,d_n (0  \leq d_i  \leq 100)\) — the elements of the array \(d\).

It can be shown that there always exists at least one suitable array a under these constraints.

\vspace{0.2cm}

\textbf{Output}\\
For each test case, print the elements of the array \(a\), if there is only one possible array \(a\).\\
Otherwise, print \(-1\).
\end{minipage}}
\end{center}


\newpage
\section{Data collection}

% Unfortunately the actual data is not currently available and therefore I will here show what can be done with a substitutional data set so I can explain how I think actual implementation.
% Make a search and see what data sets you may find when you do a search. Show and describe here.

\subsection{Overview}
The data collection involves splitting the developers in two groups for every tasks. Half of the developers will be able to use GitHub CoPilot and the other half will not have access to it.\\
This procedure will be repeated as many time as the available tasks. At the end of the experiment every developer will have coded half of the time aided by the AI. In order to mitigate the variability without overburden the developers the number of tasks to solve has been set to ten (n = 10).

\begin{center}
\captionsetup{type=figure}
\begin{tikzpicture}[node distance=2cm]

\node (start) [startstop] {Start};
\node (in1) [io, below of=start, align=center] {n = number of tasks to be solved\\c (counter) = 0};
\node (pro1) [process, below of=in1, align=center] {Split developers\\in 2 groups};
\node (pro2) [process, below of=pro1, align=center] {Collect execution\\times};
\node (pro3) [process, below of=pro2, align=center] {Compute mean time\\of the 2 groups};
\node (pro4) [process, below of=pro3, align=center] {c = c + 1};
\node (dec1) [decision, below of=pro4, yshift=-0.4cm] {c < n};
\node (pro5) [io, below of=dec1, yshift=-0.4cm, align=center] {Save computed mean times};
\node (stop) [startstop, below of=pro5] {Stop};


\draw [arrow] (start) -- (in1);
\draw [arrow] (in1) -- (pro1);
\draw [arrow] (pro1) -- (pro2);
\draw [arrow] (pro2) -- (pro3);
\draw [arrow] (pro3) -- (pro4);
\draw [arrow] (pro4) -- (dec1);
\draw [arrow] (dec1.east) -- ++(3,0) |- node[near start, anchor=west] {yes} (pro1);
\draw [arrow] (dec1) -- node[anchor=west] {no} (pro5);
\draw [arrow] (pro5) -- (stop);

\end{tikzpicture}
\captionof{figure}{Data collection overview} 
\label{fig:data overview}
\end{center}

\newpage
\subsection{Details}

For every task the developer will be shown its description and given five minutes to think on how to solve it. Once he is ready a timer is started and then stopped when the task has been completed.
The time used to solve the task is then computed and stored together with a boolean value indicating if the add aceess to GitHub CoPilot or not.

\begin{center}
\captionsetup{type=figure}
\begin{tikzpicture}[node distance=2cm]

\node (start) [startstop] {Start};
\node (in1) [io, below of=start, align=center] {GitHub CoPilot enbled?};
\node (pro1) [process, below of=in1, align=center] {Show the task to the developer};
\node (pro2) [process, below of=pro1, align=center] {Start timer};
\node (pro3) [process, below of=pro2, align=center] {The developer is done coding};
\node (pro4) [process, below of=pro3, align=center] {Stop timer};
\node (dec1) [process, below of=pro4] {Coding time = stop time - start time};
\node (pro5) [io, below of=dec1, align=center] {Save coding time together\\with GitHub CoPilot status};
\node (stop) [startstop, below of=pro5] {Stop};


\draw [arrow] (start) -- (in1);
\draw [arrow] (in1) -- (pro1);
\draw [arrow] (pro1) -- (pro2);
\draw [arrow] (pro2) -- (pro3);
\draw [arrow] (pro3) -- (pro4);
\draw [arrow] (pro4) -- (dec1);
\draw [arrow] (dec1) -- (pro5);
\draw [arrow] (pro5) -- (stop);

\end{tikzpicture}
\captionof{figure}{Data collection details} 
\label{fig:data details}
\end{center}

% \subsection{Synthetic data, simulation, data access}
% Here you may have to explain even further so we know how you plan for this.

\newpage
\printacronyms[]

\newpage
\nocite{*}
\printbibliography[]

\newpage
\section{Appendix}

\subsection{Opposition Feedback}
The feedback I received from my classmate concerned mostly technical aspects like the numbering of the references and that some of them were not cited in the text. Another feedback concerned the keywords that were deemed good but insufficient in number, to fix that I added two more keywords. Lastly the fact that I asked questions in my hypotheses was considered unusual and probably wrong therefore I removed them as they did not contribute to the hypotheses themselves.

\subsection{Timeline}
\begin{center}
\captionsetup{type=table}
\begin{tabular}{ c c }
\hline
Month & To Do \\
\hline
\hline
January & Define the final target and scope of the thesis \\
\hline
February & Work on the implementation \\
\hline
March & Collect and analyze the data \\
\hline
April & Finish the first revision of the thesis \\
\hline
May & Thesis review, submit draft to the supervisor and implement the necessary fixes \\
\hline
June & Thesis completed, prepare for the oral examination \\
\hline
\end{tabular}
\captionof{table}{Execution timeline}
\label{tab:to do}
\end{center}


\end{document}